/**
 * v 1.0
 * Simple JS Tabs System
 * ---------------------
 */

 /**
  *
  * Example
  *
  * <div>
  *  <span data-tab-trigger="tab1">Tab 1</span>
  *  <span data-tab-trigger="tab2">Tab 2</span>
  * </div>
  *
  *
  * <div>
  *  <div data-tab="tab1" data-tab-accordion>
  *   <div data-tab="tab1" data-tab-accordion-trigger>
  *     Tab 1
  *   </div>
  *   <div data-tab="tab1" data-tab-accordion-content>
  *     Tab Content
  *   </div>
  *  </div>
  *
  * 
  *  <div data-tab="tab2" data-tab-accordion>
  *   <div data-tab="tab2" data-tab-accordion-trigger>
  *     Tab 2
  *   </div>
  *   <div data-tab="tab2" data-tab-accordion-content>
  *     Tab Content
  *   </div>
  *  </div>
  * </div>
  */

/**
 * Tabs initialization
 * 
 * @return void
 */
flashCore.prototype.tabs = function(){
	// Hiding Tabs
	document.querySelectorAll('[data-tab]:not(.js_init--flashTabs)').forEach(function(node){
		node.style.display = 'none';
	});

	// Loading active Tabs
	document.querySelectorAll('[data-tab-target]:not(.js_init--flashTabs)').forEach(function(node) {
		if(node.classList.contains('active')) {
			document.querySelectorAll('[data-tab="' + node.getAttribute('data-tab-target') + '"]').forEach(function(tab){
				tab.style.display = '';
			});
			// Dispatching event to let other elements that tabs
			// are updated
			var flashReadyEvent = new CustomEvent('flashTabsUpdated');
			node.parentNode.parentNode.dispatchEvent(flashReadyEvent);
		}
	});

	// Loading active Tabs
	flash.listen(document.querySelectorAll('[data-tab-target]:not(.js_init--flashTabs)'), 'click', function(e){
		e.preventDefault();
		this.parentNode.parentNode.querySelectorAll('.active[data-tab-target]').forEach(function(trigger){
			trigger.classList.remove('active');
		});
		this.classList.add('active');
		flashTabsUpdate(this);
	});

	// Current tab on page load if url hash is not empty
	if(window.location.hash) {
		document.querySelectorAll('[data-tab-target="' + window.location.hash.replace('#', '') + '"]').forEach(function(trigger){
			trigger.click();
		});
	}

	// Marking elements as initialised
	document.querySelectorAll('[data-tab]:not(.js_init--flashTabs), [data-tab-target]:not(.js_init--flashTabs)').forEach(function(node){
		node.classList.add('js_init--flashTabs');
	});

	// Accordion on mobile
	flash.listen(document.body, 'click',  function(event){
		var tab = event.target.closest('[data-tab-accordion]');
		tab.classList.add('ignore_tab');
		document.querySelectorAll('[data-tab-accordion].open:not(.ignore_tab)').forEach(function(tab_to_close){
			tab_to_close.classList.remove('open');
			flash.slideToggle(tab_to_close.querySelector('[data-tab-accordion-content]'));
		});
		tab.classList.remove('ignore_tab');
		tab.classList.toggle('open');
		flash.slideToggle(tab.querySelector('[data-tab-accordion-content]'));
	}, '[data-tab-accordion-trigger]');


	/**
	 * Update specific tabs
	 * 
	 * @return void
	 */
	function flashTabsUpdate(clicked_tab) {
		var tab = document.querySelectorAll('[data-tab="' + clicked_tab.getAttribute('data-tab-target') + '"]');

		// This part of the code is for supporting multiple tab groups on the page
		// It searches for the parent of the group of tabs and then toggles just the tabs of this group
		var tab_group = clicked_tab.parentNode;
		// Searches for the parent of the group, so it goes up one level in the DOM
		// until it finds more than one [data-tab-target] element in the children of the
		// current object
		while(tab_group.querySelectorAll('[data-tab-target]').length == 1 && !tab_group.matches('body')) {
			tab_group = tab_group.parentNode;
		}
		// Toggles just the tabs in the current group
		tab_group.querySelectorAll('[data-tab-target]').forEach(function(el) {
			document.querySelectorAll('[data-tab="' + el.getAttribute('data-tab-target') + '"]').forEach( function(node){
				node.style.display = 'none';
			});
		});
		tab_group.querySelectorAll('[data-tab-target]').forEach( function(node){
			node.classList.remove('active');
		});
		tab_group.querySelectorAll('[data-tab-target="' + clicked_tab.getAttribute('data-tab-target') + '"]').forEach( function(node){
			node.classList.add('active');
		});
		tab.forEach(function(node){
			flash.fadeIn(node);
		});

		// Dispatching event to let other elements that tabs
		// are updated
		var flashReadyEvent = new CustomEvent('flashTabsUpdated');
		tab_group.dispatchEvent(flashReadyEvent);
	};
}