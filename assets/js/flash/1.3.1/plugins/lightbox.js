"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([], function () {
      return factory(root);
    });
  } else if ((typeof exports === "undefined" ? "undefined" : _typeof(exports)) === 'object') {
    module.exports = factory(root);
  } else {
    root.lightbox = factory(root);
  }
})(typeof global !== 'undefined' ? global : typeof window !== 'undefined' ? window : void 0, function (window) {
  'use strict';
  /**
   * DEFAULTS AND LIGHTBOX OBJECT
   * =============================================
   */

  var defaults = {
    clone_content: true,
    // If false will move the actual node in & out of the lightbox
    custom_class: '',
    // Optional classes for the lightbox
    detect_content_type: true,
    trigger_attribute: 'data-lightbox',
    trigger_event: 'click',
    group_selector: '[data-lightbox-group]',
    arrows_container_class: 'lightbox__arrows',
    next_arrow: '<div class="lightbox__arrow lightbox__arrow--next"></div>',
    prev_arrow: '<div class="lightbox__arrow lightbox__arrow--prev"></div>' // Storing the position of a node with a placeholder in case clone_content is false

  };
  var placeholder_node = null; // Placeholder node to put back content when clone_content is false

  var lightbox_node = null; // The node of the lightbox

  var content_node = null; // The node of the content of the lightbox

  var current_trigger = null; // The trigger clicked to open the lightbox
  // State variables

  var closing_lightbox = false;
  /**
   * PRIVATE METHODS
   * =============================================
   */

  /**
   * Get the video source and the video id for the embed 
   * @param  {String} video_url The url of the video
   * @return {{id: {String}, source: {String}}|Bool}  Return the object with the video id and the video source
   * or false if it's not recognised as a video
   */

  function getVideoData(video_url) {
    var video = {
      id: '',
      source: ''
    }; // Getting the source

    if (video_url.indexOf('yout') >= 0) {
      video.source = 'youtube';
    }

    if (video_url.indexOf('vimeo') >= 0) {
      video.source = 'vimeo';
    } // Get youtube video ID


    if (video.source == "youtube") {
      var youtube_regex = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;

      var _match = video_url.match(youtube_regex);

      video.id = _match && _match[2] ? _match[2] : '';
    } // Get Vimeo video ID


    if (video.source == "vimeo") {
      if (video_url.indexOf("clip_id") > -1) {
        var _vimeo_regex = /(clip_id=(\d+))/;

        var _match2 = video_url.match(_vimeo_regex);
      } else if (video_url.indexOf("player") > -1) {
        var _vimeo_regex2 = /player\.vimeo\.com\/video\/([0-9]*)/;

        var _match3 = video_url.match(_vimeo_regex2);

        _match3[2] = _match3[1];
      } else {
        var vimeo_regex = /(?:http|https):\/\/(www\.)?vimeo.com\/(\d+)($|\/)/;
        var match = video_url.match(vimeo_regex);
      }

      video.id = match && match[2] ? match[2] : '';
    }

    if (video.id && video.source) {
      return video;
    } else {
      return false;
    }
  }
  /**
   * Get the html associated to content for the lightbox. 
   * @param  {String} content         The conten for the lightbox
   * @param  {Bool}   detect_content_type   If true it will filter the output of the
   * content based on the content type (image url, video url, url, css selector), if false it will
   * return a plain string
   * @return {String|Node}   The actual content for the lightbox
   */


  var getHtmlFromContent = function getHtmlFromContent(content, options) {
    var content_output = {
      type: null,
      filtered_content: null,
      content: content
    }; // The output of this function
    // Check if content is undefined

    if (typeof content === 'undefined') {
      console.warn('Content is undefined');
      return;
    } // Get the content as plain text or try to detect it automatically


    if (!options.detect_content_type && typeof content === 'string') {
      // Plain text or html
      content_output.type = 'string';
      content_output.filtered_content = content;
    } else {
      // Check if the content is an image
      if (content.indexOf('http://') == 0 || content.indexOf('https://') == 0) {
        // IMAGE - Check if it's an image
        // Getting the extension
        var extension = content.split('?')[0].split('.').pop(); // List of images extensions

        var images_extensions = ['jpg', 'png', 'svg'];

        if (images_extensions.indexOf(extension) > -1) {
          content_output.filtered_content = "<img src=\"".concat(content, "\" alt=\"{alt}\" class=\"lightbox__image\" />");
          content_output.type = 'image';
        } // VIDEO - Check if it's a youtube video


        if (!content_output.type) {
          var video_data = getVideoData(content);

          if (video_data) {
            // YouTube
            if (video_data.source === 'youtube') {
              content_output.filtered_content = "<div class=\"lightbox__video\">\n                          <iframe src=\"https://www.youtube.com/embed/".concat(video_data.id, "?autoplay=1&showinfo=0\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen class=\"lightbox__video_iframe\"></iframe>\n                        </div>");
              content_output.type = 'video';
            } // Vimeo


            if (video_data.source === 'vimeo') {
              content_output.filtered_content = "<div class=\"lightbox__video\">\n                          <iframe src=\"//player.vimeo.com/video/".concat(video_data.id, "?title=0&amp;byline=0&amp;portrait=0&amp;color=96c159&amp;api=1&amp;autoplay=1&amp;player_id=video_").concat(video_data.id, "\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen class=\"lightbox__video_iframe\"></iframe>\n                        </div>");
              content_output.type = 'video';
            }
          }
        } // EXTERNAL PAGE


        if (!content_output.type) {
          content_output.type = 'url';
          content_output.filtered_content = "<div class=\"lightbox__url\">\n                          <iframe src=\"".concat(content, "\" frameborder=\"0\" class=\"lightbox__url_iframe\"></iframe>\n                        </div>");
        }
      }

      try {
        // NODE - Check if it's an html node
        if (!content_output.type && document.querySelector(content)) {
          content_output.type = 'node'; // Clone the node if clone_content is true or use the original node
          // if false

          if (options.clone_content) {
            content_output.filtered_content = document.querySelector(content).cloneNode(true);
          } else {
            content_node = document.querySelector(content); // Preparing the placeholder node

            placeholder_node = document.createElement('div');
            placeholder_node.classList.add("lightbox_content_placeholder");
            placeholder_node.style.display = 'none';
            content_node.parentNode.insertBefore(placeholder_node, content_node);
            content_output.filtered_content = content_node;
          }
        }
      } catch (e) {} // STRING - Fallback to text if no other type was detected


      if (!content_output.type) {
        content_output.type = 'string';
        content_output.filtered_content = content;
      }
    }

    return new Promise(function (resolve, reject) {
      resolve(content_output);
    });
  };
  /**
   * Get the the content from a lightbox trigger
   * @param  {Node}   trigger       The node of the trigger
   * @param  {String} trigger_attribute   The html attribute from which retrieve the content
   * @return {String} The content
   */


  var getContentFromTrigger = function getContentFromTrigger(trigger, trigger_attribute) {
    var content = {
      data: '',
      "class": ''
    };
    current_trigger = trigger;

    if (!trigger) {
      console.warn('No trigger is defined');
      return;
    } // Check if the trigger has the right
    // attribute set to retrieve the content


    var has_trigger_lightbox_attribute = trigger.hasAttribute(trigger_attribute);

    if (has_trigger_lightbox_attribute) {
      // Using the attribute set in the options
      content.data = trigger.getAttribute(trigger_attribute);
    } else if (trigger.matches('a[href]')) {
      // If it's an anchor without the trigger attribute set, get the href value
      content.data = trigger.getAttribute('href');
    } else {
      console.warn('The trigger has not any attribute set for content.');
      return;
    } // Getting optional classes


    if (trigger.hasAttribute('data-lightbox-class')) {
      content["class"] = trigger.getAttribute('data-lightbox-class');
    }

    return content;
  };
  /**
   * Prepare the lightbox and append it to the <body>
   * @return {Node} The node of the lightbox
   */


  var prepareLightbox = function prepareLightbox() {
    // Init Lightbox Div
    if (!lightbox_node) {
      lightbox_node = document.createElement('div');
      lightbox_node.classList.add("lightbox");
      lightbox_node.style.opacity = 0;
      document.body.appendChild(lightbox_node);
    }
  };
  /**
   * Add the arrows to the lightbox
   * @return {void}
   */


  var addArrows = function addArrows(options) {
    // Init empty nodes
    var next_node = null;
    var prev_node = null; // If the group option is not set and the current trigger has the group_selector set instead
    // use the group mode anyway

    if (!options.group && current_trigger && current_trigger.matches(options.group_selector)) {
      options.group = true;
    } // If it's a group and a group selector option is set


    if (options.group && options.group_selector) {
      var group_nodes = document.querySelectorAll(options.group_selector); // Add the arrows just if the group has more than 1 item

      if (group_nodes.length > 1) {
        group_nodes.forEach(function (node, index) {
          if (node.isSameNode(current_trigger)) {
            // Getting the previous node
            if (index > 0) {
              prev_node = group_nodes[index - 1];
            } else {
              prev_node = group_nodes[group_nodes.length - 1];
            } // Getting the next node


            if (index < group_nodes.length - 1) {
              next_node = group_nodes[index + 1];
            } else {
              next_node = group_nodes[0];
            }
          }
        });
      }
    } // Appending the arrows container


    var lightbox_inner = lightbox_node.querySelector('.lightbox__content_inner');

    if (next_node || prev_node) {
      var arrows_container = document.createElement('div');
      arrows_container.classList.add(options.arrows_container_class);
      lightbox_inner.insertAdjacentElement('beforeend', arrows_container);
    } // Appending the arrows


    if (prev_node) {
      arrows_container.insertAdjacentHTML('beforeend', options.prev_arrow);
      arrows_container.lastChild.addEventListener('click', function (event) {
        prev_node.click();
      });
    }

    if (next_node) {
      arrows_container.insertAdjacentHTML('beforeend', options.next_arrow);
      arrows_container.lastChild.addEventListener('click', function (event) {
        next_node.click();
      });
    }
  };
  /**
   * LIGHTBOX CONSTRUCTOR
   * =============================================
   * 
   * The actual Lightbox constructor object with public API
   * @param  {String} content   The content for the lightbox
   * @param  {Object} options   The options for the lightbox
   * @return {String} The content
   */


  var lightbox = function lightbox(content, options) {
    // Object where to store public methods
    var public_methods = {};
    
    /**
     * Prepare the lightbox and append it to the <body>
     * @param {Object} The lightbox object
     * @param {String} The html content to append into the ligthbox
     * @return {Bool} Success or not
     */

    public_methods.openLightbox = function (content) {
      prepareLightbox();
      options.custom_class += " ".concat(content["class"]); // Setup the lightbox close buttone

      lightbox_node.addEventListener('click', function (event) {
        if (!closing_lightbox && (event.target.matches('.lightbox') || event.target.matches('.lightbox__close'))) {
          public_methods.closeLightbox();
        }
      });
      public_methods.setContent(content.data).then(function () {
        var flashReadyEvent = new CustomEvent('lightboxVisible');
        document.body.dispatchEvent(flashReadyEvent);
        lightbox_node.style.opacity = 1;
        return true;
      })["catch"](function (error) {
        console.error("Something went really wrong trying to open the lightbox: ".concat(error));
        return false;
      });
    };
    /**
     * Closes the ligthbox and destroys it
     */


    public_methods.closeLightbox = function () {
      closing_lightbox = true;
      lightbox_node.style.opacity = 0;
      lightbox_node.addEventListener('transitionend', function () {
        if (closing_lightbox) {
          // If the content node was moved from its original location
          // put it back
          if (!options.clone_content && content_node) {
            placeholder_node.parentNode.insertBefore(content_node, placeholder_node);
            placeholder_node.remove();
          } // Removing the lightbox


          lightbox_node.remove(); // Resetting nodes

          lightbox_node = null;
          content_node = null;
          placeholder_node = null;
          closing_lightbox = false;
          current_trigger = null;

          // Event
          var flashClosedEvent = new CustomEvent('lightboxClosed');
          document.body.dispatchEvent(flashClosedEvent);
        }
      });
    };
    /**
     * Change the content in the lightbox
     * @param {String} content The content for the lightbox
     */


    public_methods.setContent = function (content) {
      return new Promise(function (resolve, reject) {
        getHtmlFromContent(content, options) // Get the HTML associated with the current content
        .then(function (filtered_content) {
          // Build Lightbox content
          var template = "<div class=\"lightbox__content lightbox__content--".concat(filtered_content.type, " ").concat(options.custom_class, "\">\n                                <div class=\"lightbox__close\">{close_button}</div>\n                                <div class=\"lightbox__content_inner\">\n                                  {content}\n                                </div>\n                            </div>");
          var lightbox_html = template.replace('{close_button}', options.close_button || '');

          if (filtered_content.type === 'node' && Node.prototype.isPrototypeOf(filtered_content.filtered_content)) {
            lightbox_node.innerHTML = lightbox_html.replace('{content}', '');
            lightbox_node.querySelector('.lightbox__content_inner').appendChild(filtered_content.filtered_content);
          } else {
            lightbox_html = lightbox_html.replace('{content}', filtered_content.filtered_content); // Append lightbox to the body

            lightbox_node.innerHTML = lightbox_html;
          } // Add arrows if it's the case


          addArrows(options);
          resolve();
        })["catch"](function (error) {
          reject(error);
        });
      });
    };
    /**
     * Set the options for the lightbox
     * @param {object} options The object with all the options for the lightbox
     */


    public_methods.setOptions = function () {
      // Setting options if they are not set
      if (!options) {
        options = {};
      } // Checking if options is an object


      if (_typeof(options) === 'object') {
        // Overriding options
        options = Object.assign({}, defaults, options);
      } else if (options) {
        console.warn('Options must be an object.');
      }
    };
    /**
     * Set the triggers for the lightbox based on the trigger option
     */


    public_methods.setTrigger = function () {
      if (!options.trigger) {
        return;
      } // Checking the type of target
      // nodes, nodeslists and selectors are allowed


      var nodes = '';

      if (_typeof(options.trigger) === 'object' && Node.prototype.isPrototypeOf(options.trigger)) {
        nodes.push(options.trigger);
      } else if (_typeof(options.trigger) === 'object' && NodeList.prototype.isPrototypeOf(options.trigger)) {
        nodes = options.trigger;
      } else if (typeof options.trigger === 'string') {
        nodes = options.trigger;
      } else {
        console.error('The target argument is invalid. It must be a node or a css selector.');
        return;
      } // Creating the listeners


      if (typeof nodes === 'string') {
        // If it's a selector, event delegation is used to listen to the events
        // Generate a string with the options to check if a lightbox
        // with the same options was already created
        var function_name = JSON.stringify(options);

        if (window.lightboxTriggerSetups && window.lightboxTriggerSetups.indexOf(function_name) > -1) {
          // If a lightbox was already called with the same options, just quit
          return false;
        } else {
          // Adding the current set up name to the list of the ones
          // already used
          if (!window.lightboxTriggerSetups) {
            window.lightboxTriggerSetups = [];
          }

          window.lightboxTriggerSetups.push(function_name);
        }

        document.addEventListener(options.trigger_event, function (event) {
          var current_node = event.target;

          while (current_node.parentNode) {
            if (typeof current_node.matches !== 'undefined' && current_node.matches(options.trigger)) {
              event.preventDefault();
              var ligthbox_content = getContentFromTrigger(current_node, options.trigger_attribute);
              public_methods.openLightbox(ligthbox_content);
              break;
            }

            current_node = current_node.parentNode;
          }
        });
      } else {
        // If it's a node or a nodelist, just those elements are used for the listeners
        nodes.forEach(function (node) {
          lightbox_node.addEventListener(options.trigger_event, function (event) {
            event.preventDefault();
            var ligthbox_content = getContentFromTrigger(event.target, options.trigger_attribute);
            public_methods.openLightbox(ligthbox_content);
          });
        });
      }
    }; // If no content is set and just options are passed 
    // to the lightbox


    if (_typeof(content) === 'object') {
      options = Object.assign({}, content);
      content = null;
    } // Initialise options


    public_methods.setOptions(options); // Open the lightbox or listen to triggers

    if (content) {
      // If the content is set, open the ligthbox
      public_methods.openLightbox({
        data: content
      });
    } else if (options.trigger) {
      // If no content is set, listen triggers
      public_methods.setTrigger();
    } else {
      console.warn('No content and no options were set. Are you kidding me?');
      return;
    }

    return public_methods;
  };

  return lightbox;
});